# Contributing

Please be civil.

Contributions are very welcome!

If you always wanted to try our Rust but never had an idea of what to do, this might be it!
Mentoring is available.
If you want to implement something, make a new issue and describe what you want to imlement and if you need help.

If you don't need help, then choose an issue to implement and add a comment that you want to work on it.
I only ask that you ping the issue with an update (no progress is fine as long as you are still planning to work on it),
otherwise I'll unassign the issue so someone else can pick it up.

## To submit a merge request

- Fork the project
- do the changes
- test with `cargo run` 
- open a merge request
- (if generation was somehow changed) screenshots are welcome!

Thank you!
