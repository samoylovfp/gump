# Gump

Realistic tree generator based on Weber & Penn's [paper](https://courses.cs.duke.edu/cps124/spring08/assign/07_papers/p119-weber.pdf)

See [CONTRIBUTING](CONTRIBUTING.md) if you want to participate!

## Tweaker
`cargo run`

Runs an example with a GUI, that allows tweaking the parameters and see the results live.

Online [demo](https://samoylovfp.gitlab.io/gump/)


![Example](/docs/tree.png)


## Roadmap

The goal is to fully implement the paper and add something more, broken branches, cracks, procedural textures.

Ideally a large beautiful forest can be generated quickly for any kind of game you can think of.
