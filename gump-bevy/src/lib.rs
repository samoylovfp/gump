//! Crate to make use of [gump] objects in [bevy]

use bevy::{
    prelude::*,
    render::{mesh::Indices, render_resource::PrimitiveTopology},
};

use gump::{StemConfig, Tree};

/// Generates a tree as a bevy entity with a [bevy::prelude::Mesh] attached
pub fn add_tree(
    commands: &mut Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    stem_configs: Vec<StemConfig>,
    seed: u64,
) {
    let tree = Tree::generate(&stem_configs, seed);
    let gump_mesh = tree.generate_mesh(6);

    let mesh = gump_mesh_into_new_bevy_mesh(&gump_mesh);

    commands.spawn().insert_bundle(PbrBundle {
        mesh: meshes.add(mesh),
        material: materials.add(StandardMaterial {
            base_color: Color::hex("ffff00").unwrap(),
            metallic: 0.0,
            ..Default::default()
        }),
        ..Default::default()
    });
}

/// Creates a new [bevy::prelude::Mesh] from [gump::Mesh]
pub fn gump_mesh_into_new_bevy_mesh(gump_mesh: &gump::Mesh) -> bevy::prelude::Mesh {
    let mut bevy_mesh = Mesh::new(PrimitiveTopology::TriangleList);
    gump_mesh_into_bevy_mesh(&mut bevy_mesh, &gump_mesh);
    bevy_mesh
}

/// Replaces existing [bevy::prelude::Mesh] with contents from a [gump::Mesh]
pub fn gump_mesh_into_bevy_mesh(bevy_mesh: &mut bevy::prelude::Mesh, gump_mesh: &gump::Mesh) {
    bevy_mesh.set_attribute(
        Mesh::ATTRIBUTE_POSITION,
        gump_mesh
            .vertices
            .iter()
            .map(|p| {
                // In gump +Z is up, in bevy +Y is up
                [p.x, p.z, -p.y]
            })
            .collect::<Vec<[f32; 3]>>(),
    );
    bevy_mesh.set_indices(Some(Indices::U32(
        gump_mesh.indices.iter().map(|&i| i as u32).collect(),
    )));

    bevy_mesh.set_attribute(
        Mesh::ATTRIBUTE_NORMAL,
        gump_mesh
            .normals
            .iter()
            .map(|v| [v.x, v.z, -v.y])
            .collect::<Vec<_>>(),
    );

    // FIXME: fake UV
    bevy_mesh.set_attribute(
        Mesh::ATTRIBUTE_UV_0,
        vec![[0.0, 0.0]; gump_mesh.vertices.len()],
    );
}
