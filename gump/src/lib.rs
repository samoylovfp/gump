//! Realistic tree generator based on Weber Penn [paper](https://courses.cs.duke.edu/cps124/spring08/assign/07_papers/p119-weber.pdf)
//!
//! # Examples
//! ```
//! use gump::Tree;
//! use gump::StemConfig;
//! 
//! let tree = Tree::generate(&[
//!     StemConfig::default()
//! ], 0);
//! let mesh = tree.generate_mesh(8);
//! ```

mod mesh;
mod red;
mod stem;
mod stem_config;
mod tree;

pub use mesh::Mesh;
pub use stem_config::StemConfig;
pub use tree::Tree;
