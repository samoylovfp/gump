use nalgebra::{Point3, Vector3};

#[derive(Debug)]
pub struct Mesh {
    pub vertices: Vec<Point3<f32>>,
    pub indices: Vec<usize>,
    pub normals: Vec<Vector3<f32>>,
}
