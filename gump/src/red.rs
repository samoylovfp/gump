//! This module is for functions that trigger rust_analyzer false-errors

use nalgebra::{Point3, Vector3};

pub fn p3(x: f32, y: f32, z: f32) -> Point3<f32> {
    Point3::new(x, y, z)
}

pub fn v3(x: f32, y: f32, z: f32) -> Vector3<f32> {
    Vector3::new(x, y, z)
}
