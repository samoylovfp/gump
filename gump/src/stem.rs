use std::f32::consts::TAU;

use nalgebra::{Similarity3, Point3, Rotation3, Vector3};

use crate::red::*;

/// Circular cut of the stem
#[derive(Debug, Clone, Copy)]
pub struct StemPart {
    pub direction: Similarity3<f32>,
}

impl StemPart {
    fn get_vertex(&self, index: isize, of: usize) -> Point3<f32> {
        let circle_point = p3(1.0, 0.0, 0.0);
        let rotated = Rotation3::from_euler_angles(0.0, 0.0, index as f32 / of as f32 * TAU)
            .transform_point(&circle_point);
        self.direction.transform_point(&rotated)
    }
}

#[derive(Debug)]
pub struct Stem {
    pub parts: Vec<StemPart>,
}

impl Stem {
    pub fn generate_mesh(
        &self,
        vertices: &mut Vec<Point3<f32>>,
        indices: &mut Vec<usize>,
        normals: &mut Vec<Vector3<f32>>,
        resolution: usize,
    ) {
        let index_offset = vertices.len();
        // south -> north
        for (y, current_part) in self.parts.iter().enumerate() {
            // west -> east
            for x in 0..resolution {
                let x = x as isize;
                let current_vertex = current_part.get_vertex(x, resolution);

                let west_vertex = current_part.get_vertex(x - 1, resolution);
                let east_vertex = current_part.get_vertex(x + 1, resolution);

                let east_edge = east_vertex - current_vertex;
                let west_edge = west_vertex - current_vertex;

                let mut current_vertex_normal = Vector3::zeros();

                if y > 0 {
                    let south_vertex = self.parts[y - 1].get_vertex(x, resolution);
                    let south_edge = south_vertex - current_vertex;
                    current_vertex_normal += south_edge.cross(&east_edge);
                    current_vertex_normal += west_edge.cross(&south_edge);
                }

                if y < self.parts.len() - 1 {
                    let north_vertex = self.parts[y + 1].get_vertex(x, resolution);
                    let north_edge = north_vertex - current_vertex;
                    current_vertex_normal += east_edge.cross(&north_edge);
                    current_vertex_normal += north_edge.cross(&west_edge);
                }

                vertices.push(current_vertex);
                normals.push(current_vertex_normal);
            }
        }

        for y in 0..self.parts.len() - 1 {
            for x in 0..resolution {
                let current = y * resolution + x;
                let north = (y + 1) * resolution + x;
                let west = y * resolution + (x + 1) % resolution;
                let north_west = (y + 1) * resolution + (x + 1) % resolution;
                indices.extend(
                    [current, west, north, west, north_west, north]
                        .into_iter()
                        .map(|x| x + index_offset),
                );
            }
        }
    }
}