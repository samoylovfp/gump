/// Config of stem generation on a specific level of recursion
#[derive(Clone, Copy, Debug)]
pub struct StemConfig {
    /// amount of cross-cuts
    pub curve_res: usize,
    /// how strong the stem curves (radians)
    pub curve: f32,
    /// Vertical axis rotation
    pub rotate: f32,
    /// Deviation from the vertical
    pub down_angle: f32,
    /// magnitude of random rotation
    pub curve_var: f32,
    /// how conical the shape is (0.0 - cylinder, 1.0 - conus)
    pub taper: f32,
    /// how many children stems this level has
    pub n_branches: usize,
    /// Overall tallness of the stem
    pub length: f32,
    /// Fraction of the stem that does not have any children [0.0; 1.0]
    pub base_size: f32,
}

impl Default for StemConfig {
    fn default() -> Self {
        Self {
            curve_res: 10,
            curve: 0.5,
            rotate: 1.5,
            down_angle: 0.75,
            curve_var: 0.1,
            taper: 1.0,
            n_branches: 3,
            length: 30.0,
            base_size: 0.3,
        }
    }
}
