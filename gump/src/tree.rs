use nalgebra::{Rotation3, Similarity3, UnitQuaternion, Vector3};
use rand::{prelude::SmallRng, Rng, SeedableRng};

use crate::{
    mesh::Mesh,
    red::*,
    stem::{Stem, StemPart},
    stem_config::StemConfig,
};

/// Collection of stems and leaves
/// Is used to generate the mesh
/// +Z is up, +X is right, +Y is out of the screen
#[derive(Debug)]
pub struct Tree {
    pub stems: Vec<Stem>,
}

impl Tree {
    pub fn generate(stem_configs: &[StemConfig], seed: u64) -> Self {
        let mut randomizer = SmallRng::seed_from_u64(seed);
        let mut stems = vec![];
        let mut rand32 = || randomizer.gen::<f32>() * 2.0 - 1.0;

        Tree::generate_stems(
            &mut rand32,
            v3(0.0, 0.0, 0.0),
            UnitQuaternion::identity(),
            stem_configs,
            0,
            &mut stems,
            1.0,
        );

        Tree { stems }
    }

    fn generate_stems(
        rand32: &mut impl FnMut() -> f32,
        mut cursor: Vector3<f32>,
        mut current_rotation: UnitQuaternion<f32>,
        stem_config: &[StemConfig],
        current_level: u8,
        stems: &mut Vec<Stem>,
        current_stem_scale: f32,
    ) {
        let mut current_stem_parts = vec![];
        let config = stem_config[current_level as usize];
        let per_segment_curvature = config.curve / config.curve_res as f32;
        let mut child_rotate = UnitQuaternion::<f32>::identity();

        // loop by branch segments
        for curve_index in 0..=config.curve_res {
            // FIXME current_stem_scale is a kludge
            let width = current_stem_scale * (config.curve_res - curve_index) as f32
                / std::cmp::max(config.curve_res, 1) as f32;

            let stem_progress = curve_index as f32 / config.curve_res as f32;

            current_stem_parts.push(StemPart {
                direction: Similarity3::from_parts(
                    cursor.into(),
                    current_rotation,
                    width.max(0.00001),
                ),
            });

            current_rotation *= Rotation3::from_euler_angles(
                per_segment_curvature + rand32() * config.curve_var,
                rand32() * config.curve_var,
                rand32() * config.curve_var,
            );

            let stem_length = config.length * current_stem_scale / config.curve_res.max(1) as f32;

            let growth_vector = v3(0.0, 0.0, stem_length);
            cursor += current_rotation.transform_vector(&growth_vector);

            // Don't recurse on the last level of stems
            if (current_level as usize) == stem_config.len() - 1 {
                continue;
            }
            // Don't make a child stem from the top of the current stem
            if curve_index == config.curve_res - 1 {
                continue;
            }

            let down_angle = Vector3::x() * config.down_angle;
            let rotate = Vector3::z() * config.rotate;
            child_rotate *= UnitQuaternion::new(rotate);
            // TODO implement proper branch scale calculation
            let branch_scaling = 0.75;
            if stem_progress >= config.base_size {
                Tree::generate_stems(
                    rand32,
                    cursor,
                    current_rotation * child_rotate * Rotation3::new(down_angle),
                    stem_config,
                    current_level + 1,
                    stems,
                    width * branch_scaling,
                );
            }
        }

        stems.push(Stem {
            parts: current_stem_parts,
        });
    }

    pub fn generate_mesh(&self, resolution: usize) -> Mesh {
        let mut vertices = Vec::with_capacity(
            resolution * self.stems.iter().map(|s| s.parts.len()).sum::<usize>(),
        );
        let mut indices = Vec::with_capacity(vertices.capacity());
        let mut normals = Vec::with_capacity(vertices.capacity());

        for stem in &self.stems {
            stem.generate_mesh(&mut vertices, &mut indices, &mut normals, resolution);
        }

        Mesh {
            normals,
            vertices,
            indices,
        }
    }
}
