use std::{
    f32::consts::{PI, TAU},
    time::Duration,
};

use instant::Instant;

use bevy::{
    ecs::system::QuerySingleError,
    prelude::*,
    render::{mesh::Indices, render_resource::PrimitiveTopology},
};
use bevy_egui::{EguiContext, EguiPlugin};
use bevy_fly_camera::{FlyCamera, FlyCameraPlugin};

use egui::Slider;
use gump::StemConfig;
use gump_bevy::gump_mesh_into_bevy_mesh;

#[derive(Default)]
struct TreeConfig {
    stems: Vec<StemConfig>,
    seed: u64,
}

struct TreeRenderStats {
    time: Option<Duration>,
    vertex_count: usize,
}

fn main() {
    color_backtrace::install();
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(FlyCameraPlugin)
        .add_system(camera_freezer)
        .add_plugin(EguiPlugin)
        .add_system(bevy::input::system::exit_on_esc_system)
        .add_system(tree_tweaker)
        .add_startup_system(init)
        .add_startup_system(ground)
        .insert_resource(ClearColor(Color::hex("6bc6ea").unwrap()))
        .insert_resource(TreeRenderStats {
            time: None,
            vertex_count: 0,
        })
        .insert_resource(default_tree_config())
        .run();
}

fn default_tree_config() -> TreeConfig {
    let stems = vec![StemConfig::default(); 3];
    TreeConfig { stems, seed: 0 }
}

fn init(mut commands: Commands) {
    commands.spawn_bundle(DirectionalLightBundle {
        transform: Transform::from_translation(Vec3::new(10.0, 50.0, 10.0))
            .looking_at(Vec3::ZERO, Vec3::Y),
        directional_light: DirectionalLight {
            illuminance: 3_000.,
            shadows_enabled: true,
            // shadow_depth_bias: 0.1,
            ..Default::default()
        },
        ..Default::default()
    });
    set_up_camera(&mut commands);
}

#[derive(Component)]
struct Tree;

fn tree_tweaker(
    mut commands: Commands,
    mut tree_config: ResMut<TreeConfig>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut render_stats: ResMut<TreeRenderStats>,
    trees: Query<&Handle<Mesh>, With<Tree>>,
    egui_context: ResMut<EguiContext>,
) {
    let mut data_changed = false;

    egui::Window::new("Tree config").show(egui_context.ctx(), |ui| {
        ui.label("Right click to (un)freeze the camera");
        ui.hyperlink("https://gitlab.com/samoylovfp/gump/");
        ui.horizontal(|ui| {
            ui.label("seed");
            data_changed |= ui.add(Slider::new(&mut tree_config.seed, 0..=20)).changed();
        });

        for (i, config) in tree_config.stems.iter_mut().enumerate() {
            ui.collapsing(format!("Level {}", i), |ui| {
                ui.horizontal(|ui| {
                    ui.label("Curve res");
                    data_changed |= ui
                        .add(Slider::new(&mut config.curve_res, 1..=100))
                        .changed();
                });
                ui.horizontal(|ui| {
                    ui.label("Curve");
                    data_changed |= ui.add(Slider::new(&mut config.curve, -PI..=PI)).changed();
                });
                ui.horizontal(|ui| {
                    ui.label("Curve var");
                    data_changed |= ui
                        .add(Slider::new(&mut config.curve_var, 0.0..=PI / 2.0))
                        .changed();
                });

                ui.horizontal(|ui| {
                    ui.label("Down angle");
                    data_changed |= ui
                        .add(Slider::new(&mut config.down_angle, 0.0..=PI / 2.0))
                        .changed();
                });

                ui.horizontal(|ui| {
                    ui.label("Rotate");
                    data_changed |= ui.add(Slider::new(&mut config.rotate, 0.0..=TAU)).changed();
                });
                ui.horizontal(|ui| {
                    ui.label("Length");
                    data_changed |= ui
                        .add(Slider::new(&mut config.length, 1.0..=50.0))
                        .changed();
                });
            });
        }
        if let Some(d) = render_stats.time {
            ui.label(format!("Tree calculated in {:?}", d));
            ui.label(format!("Tree has {} vertices", render_stats.vertex_count));
        }
    });

    let mesh_handle = match trees.get_single() {
        Ok(s) => s.clone(),
        Err(QuerySingleError::NoEntities(..)) => {
            let mesh_handle = meshes.add(Mesh::new(PrimitiveTopology::TriangleList));
            commands.spawn().insert(Tree).insert_bundle(PbrBundle {
                mesh: mesh_handle.clone(),
                material: materials.add(StandardMaterial {
                    base_color: Color::hex("9e6101").unwrap(),
                    metallic: 0.0,
                    perceptual_roughness: 0.9,
                    reflectance: 0.5,
                    ..Default::default()
                }),
                ..Default::default()
            });
            mesh_handle
        }
        Err(e) => panic!("{:?}", e),
    };
    let mesh = meshes.get_mut(mesh_handle).unwrap();

    if data_changed || mesh.count_vertices() == 0 {
        let start = Instant::now();
        let tree = gump::Tree::generate(&tree_config.stems, tree_config.seed);
        let gump_mesh = tree.generate_mesh(6);
        gump_mesh_into_bevy_mesh(mesh, &gump_mesh);
        *render_stats = TreeRenderStats {
            time: Some(start.elapsed()),
            vertex_count: mesh.count_vertices(),
        };
    }
}

fn set_up_camera(commands: &mut Commands) {
    let camera = PerspectiveCameraBundle {
        transform: Transform::from_xyz(0.0, 20.0, 50.0)
            .looking_at(Vec3::new(0.0, 6.0, 0.0), Vec3::Y),
        ..Default::default()
    };
    commands.spawn_bundle(camera).insert(FlyCamera {
        enabled: false,
        ..Default::default()
    });
}

fn camera_freezer(mut camera: Query<&mut FlyCamera>, mouse_button: Res<Input<MouseButton>>) {
    for mut cam in camera.iter_mut() {
        if mouse_button.just_pressed(MouseButton::Right) {
            cam.enabled = !cam.enabled;
        }
    }
}

fn ground(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let mut ground = Mesh::new(PrimitiveTopology::TriangleStrip);
    const GROUND_SIZE: f32 = 100.0;

    ground.set_attribute(
        Mesh::ATTRIBUTE_POSITION,
        vec![
            [-GROUND_SIZE, -0.0, -GROUND_SIZE],
            [-GROUND_SIZE, -0.0, GROUND_SIZE],
            [GROUND_SIZE, -0.0, -GROUND_SIZE],
            [GROUND_SIZE, -0.0, GROUND_SIZE],
        ],
    );
    ground.set_indices(Some(Indices::U32(vec![0, 1, 2, 1, 2, 3])));
    ground.set_attribute(Mesh::ATTRIBUTE_UV_0, vec![[0.0, 0.0]; 4]);
    ground.set_attribute(Mesh::ATTRIBUTE_NORMAL, vec![[0.0, 1.0, 0.0]; 4]);

    let mesh_handle = meshes.add(ground);
    commands.spawn().insert_bundle(PbrBundle {
        mesh: mesh_handle,
        material: materials.add(StandardMaterial {
            base_color: Color::hex("7eff41").unwrap(),
            metallic: 0.0,
            perceptual_roughness: 1.0,
            ..Default::default()
        }),
        ..Default::default()
    });
}
